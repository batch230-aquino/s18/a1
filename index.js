console.log("Hello Mundo");

function printAddition(x, y) {
  let totalSumValue = x + y;
  console.log("Display sum of 5 and 15");
  console.log(totalSumValue);
}

printAddition(5, 15);

function printSubstraction(x, y) {
  let totalSubstractionValue = x - y;
  console.log("Display product of 20 and 5");
  console.log(totalSubstractionValue);
}

printSubstraction(20, 5);

function printMultiplication(firstInput, SecondInput) {
  let MultiplicationFormula = firstInput * SecondInput;
  console.log("The product of 50 and 10:");
  return MultiplicationFormula;
}

let totalProduct = printMultiplication(50, 10);
console.log(totalProduct);

function printDivision(firstInput, SecondInput) {
  let DivisionFormula = firstInput / SecondInput;
  console.log("The quotient of 50 and 10:");
  return DivisionFormula;
}

let totalQuotient = printDivision(50, 10);
console.log(totalQuotient);

// Circle Formula
function totalCircleArea(pi, radius) {
  let circleFormula = pi * radius ** 2;
  console.log("The Result of getting the area of circle with 15 radius:");
  return circleFormula;
}

let circleArea = totalCircleArea(3.1415, 15);
let roundTotalArea = circleArea.toFixed(2);
console.log(roundTotalArea);

// Total Average
function averageScore(firstScore, secondScore, thridScore, fourthScore) {
  let totalAverage = (firstScore + secondScore + thridScore + fourthScore) / 4;
  console.log("Total avrage of 20,40,60 and 80:");
  return totalAverage;
}

let totalScore = averageScore(20, 40, 60, 80);
console.log(totalScore);

// Percentage Score
function checkPassingScore(score, total) {
  let scoreGrade = (score / total) * 100 >= 75;
  console.log("is 38/50 passing score? ");
  return scoreGrade;
}
let isPassed = checkPassingScore(38, 50);
console.log(isPassed);
